
% some parameters
folder_imgs = 'eyebrows/black_bg_bmp/';
results_folder = 'extracted-eb';
extension = '.bmp';

% list all 'extension' files in 'folder_imgs'
eb_files = dir([folder_imgs '*' extension]);
n_eb_files = numel(eb_files);

% start the loop for every image in the list
for nf=1:n_eb_files
    
    % % Image file name
    img_name = eb_files(nf).name;
    
    % % Load image
    img = imread(fullfile(folder_imgs, img_name));
    
    % % Load mask
    mask_name = strrep(img_name, '.bmp', '_final_mask.mat');
    mask_img = load(fullfile(folder_imgs, mask_name));
    mask = mask_img.final_mask;
    
    mask_bmp = img(:,:,1)~=0;
    
    if(sum(mask_bmp(:)-mask(:)))
        disp(['Masks different: ' num2str(nf)]);
        figure; imshowpair(mask, mask_bmp), title(num2str(nf))
    else
        disp(['Image ' num2str(nf) ' OK']);
    end
end
    
   