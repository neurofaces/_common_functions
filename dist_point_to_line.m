function d = dist_point_to_line(pt, v1, v2)

if(numel(pt)<3), pt = [pt 0]; end
if(numel(v1)<3), v1 = [v1 0]; end
if(numel(v2)<3), v2 = [v2 0]; end


a = v1 - v2;
b = pt - v2;
d = norm(cross(a,b)) / norm(a);