function colornames_diff(scheme,rgb)
% Create a figure to compare the color distance methods of "colornames.m" (deltaE).
%
% (c) 2015 Stephen Cobeldick
%
% Syntax:
%  colornames_diff(scheme,colormap)
%
% Creates a figure displaying the supplied colormap as horizontal color bands,
% overlaid with columns of the closest matching named colors taken from the
% provided colorscheme. Each column represents one color difference method (deltaE).
%
% For more information on color difference concepts and formulae:
% https://en.wikipedia.org/wiki/Color_difference
%
% ### Examples ###
%
% colornames_diff('html',jet(18))
%
% colornames_diff('x11',summer(18))
%
% colornames_diff('matlab',jet(18))
%
% ### Input Arguments ###
%
% scheme = String, the name of a color scheme supported by "colornames".
% colormap = NumericArray, size Nx3, each row is an RGB triple (0<=rgb<=1).
%
% colornames_diff(scheme,colormap)

persistent fgh axh pth txh lbh
%
% Text lightness threshold:
thr = 0.54;
%
if isempty(fgh)||~ishghandle(fgh)
    fgh = figure('HandleVisibility','callback', 'IntegerHandle','off',...
        'NumberTitle','off', 'Name','ColorNames DeltaE', 'Color','white');
    axh = axes('Parent',fgh, 'Visible','off', 'XTick',[], 'YTick',[],...
        'Units','normalized', 'Position',[0,0,1,1]);
else
    delete(pth)
    delete(lbh)
    delete([txh{:}])
end
%
assert(ismatrix(rgb)&&size(rgb,2)==3&&isreal(rgb)&&all(0<=rgb(:)&rgb(:)<=1),...
    'Second input argument can be a colormap of RGB values (size Nx3).')
colormap(axh,rgb);
%
N = size(rgb,1);
x = [0;0;1;1];
y = [0;1;1;0];
X = repmat(x,1,N);
Y = bsxfun(@plus,y,0:N-1);
pth = patch(X,Y,1:N, 'Parent',axh, 'EdgeColor','none', 'FaceColor','flat',...
    'CDataMapping','direct');
%
typ = {'CIE94','CIE76','CMC2:1','RGB'};
Tn = numel(typ);
[Nam,RGB] = cellfun(@(t)colornames(scheme,rgb,t),typ, 'UniformOutput',false);
BAW = cellfun(@(c)thr>c*[0.298936;0.587043;0.114021],RGB, 'UniformOutput',false);
%
fun = @(s,c,b,n) text((2*n-1)*ones(1,N)/(2*Tn), mean(Y.',2), zeros(1,N), s,...
    'Parent',axh, 'HorizontalAlignment','center',...
    {'BackgroundColor'},num2cell(c,2), {'Color'},num2cell(b(:,[1,1,1]),2));
txh = cellfun(fun, Nam, RGB, BAW, num2cell(1:Tn), 'UniformOutput',false);
%
set(axh,'YLim',[0,N+1]);
lbh = text((1:2:2*Tn)/(2*Tn), N+ones(1,Tn)/2, zeros(1,Tn), typ(:),...
    'Parent',axh, 'HorizontalAlignment','center', 'Color','black');
%
end
%----------------------------------------------------------------------END:colornames_diff