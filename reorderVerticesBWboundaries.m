function vertices=reorderVerticesBWboundaries(boundaries)
    x = boundaries(:,2);
    y = boundaries(:,1);
    nl = numel(x)*2;
    
    vertices = zeros(1, nl);
    vertices(1:2:end) = x;
    vertices(2:2:end) = y;