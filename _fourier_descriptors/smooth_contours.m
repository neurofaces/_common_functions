function imgo = smooth_contours(img, n_fd)
% Use Fourier Descriptors to smooth the shape's contour
%   img is a bw image containing the border of the shape
%   n_fd is the number of FD to keep

h = size(img, 1);
w = size(img, 2);

imgo = zeros(h, w);

try
    boundaries = bwboundaries(img);
    n_boundaries = numel(boundaries);
    for nb=1:n_boundaries
        z = frdescp(boundaries{nb});
        zinv = ifrdescp(z, n_fd);
        ind = sub2ind([h w],round(zinv(:,1)),round(zinv(:,2)));
        imgo(ind) = 1;
    end
catch e
    % output is imgs all with zeros
    disp('ERROR: An error occurred while performing smoothing.');
    disp(getReport(e));
end