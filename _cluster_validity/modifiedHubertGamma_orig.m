%Calculate the modified Hubert Gamma and normalized modified
%Hubert Gamma statistics.
function output = modifiedHubertGamma(labels, points)
numClusters = max(labels);
numPoints = length(labels);
clusterPoints = ones(numClusters, 1);
xcenters = zeros(numClusters, 1);
ycenters = zeros(numClusters, 1);
%Find the center of each cluster
for i = 1:length(labels)
    current = labels(i);
    clusterPoints(current) = clusterPoints(current) + 1;
    xcenters(current) = xcenters(current) + points(i, 1);
    ycenters(current) = ycenters(current) + points(i, 2);
end
xcenters = xcenters ./ clusterPoints;
ycenters = ycenters ./ clusterPoints;
%P is the proximity matrix
P = squareform(pdist(points)); %distances
P = max(max(P)) - P; %similarities
%QQ(i, j) is the distance between cluster i and j
QQ = zeros(numClusters);
for i = 1:numClusters
    for j = 1:numClusters
        QQ(i, j) = sqrt((xcenters(i) - xcenters(j))^2 + (ycenters(i) - ycenters(j))^2);
    end
end
%Q(i, j) is the distance between the center of the cluster to which
%point i belongs and the center of the cluster to which point j belongs.
Q = zeros(size(P));
for i = 1:numPoints - 1
    for j = i + 1:numPoints
        Q(i, j) = QQ(labels(i), labels(j));
    end
end
hubert = 0;
normHubert = 0;
%Find the mean of the upper right diagonal of P and Q
% sumP = 0;
% sumQ = 0;
% for i = 1:numPoints - 1
%     for j = i + 1:numPoints
%         sumP = sumP + P(i, j);
%         sumQ = sumQ + Q(i, j);
%     end
% end
% faster and more efficient
n_points_den = (numPoints * (numPoints - 1) / 2);
sumP = sum(sum(triu(P, 1)));
sumQ = sum(sum(triu(Q, 1)));
meanP = sumP / n_points_den;
meanQ = sumQ / n_points_den;
meanPsq = meanP^2;
meanQsq = meanQ^2;
%Find the variance of the upper right diagonal of P and Q
% stdevP = 0;
% stdevQ = 0;
% for i = 1:numPoints - 1
%     for j = i + 1:numPoints
%         stdevP = stdevP + P(i, j)^2 - meanPsq;
%         stdevQ = stdevQ + Q(i, j)^2 - meanQsq;
%     end
% end
% faster and more efficient
stdevP = sum(sum(triu(P.^2 - meanPsq, 1)));
stdevQ = sum(sum(triu(Q.^2 - meanQsq, 1)));
stdevP = stdevP / n_points_den;
stdevQ = stdevQ / n_points_den;
varP = sqrt(stdevP);
varQ = sqrt(stdevQ);
for i = 1:numPoints - 1
    for j = i + 1:numPoints
        hubert = hubert + P(i, j) * Q(i, j);
        normHubert = normHubert + (P(i, j) - meanP) *(Q(i, j) - meanQ);
    end
end
hubert = hubert / n_points_den;
normHubert = normHubert / (numPoints * (numPoints - 1)/ 2);
normHubert = normHubert / (varP * varQ);
output = [hubert, normHubert];