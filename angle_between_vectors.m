function angle = angle_between_vectors(v1, v2)

% if(numel(v1)<3), v1 = [v1 0]; end
% if(numel(v2)<3), v2 = [v2 0]; end
% 
% % ebr tilt
% ang = atan2(norm(cross(v1,v2)),dot(v1,v2));
% angle = mod(180/pi * ang, 360);

ang = atan2(v1(1)*v2(2)-v2(1)*v1(2),v1(1)*v2(1)+v1(2)*v2(2));
angle = mod(-180/pi * ang, 360);