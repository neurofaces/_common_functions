g=imdilate(mask,strel('disk',1))-imerode(mask,strel('disk',1));
g2=g*mean(img_rnu(mask>0));
img_rnu2=double(img_rnu).*mask+g2;
img_rnu2=double(img_rnu)+g2;
imc=close_holes(img_rnu2);
imo=double(imc)-double(img_rnu2);
imo2=imo.*imerode(mask,strel('disk',2));