function sizes = find_sizes_of_all_images(folder_imgs, img_filenames, extension, recompute)
% This is just to find the MODE of H and W of all images

if(exist('eye_images_sizes.mat', 'file') && ~exist('recompute', 'var'))
    load('eye_images_sizes.mat')
    return;
end

n_img_filenames = numel(img_filenames);

hw = waitbar(0, 'Getting images size... 0%');
t=tic;

% This is just to find the MODE of H and W of all images
sizes = zeros(n_img_filenames, 2);
for nf=1:n_img_filenames
    waitbar(nf/n_img_filenames, hw, sprintf('Getting images size... %.2f%%', nf/n_img_filenames*100));
    
    % % Image filename
    filename = strrep(img_filenames(nf).name, extension, '');
    
    % % Image info
    img = imfinfo(fullfile(folder_imgs, [filename extension]));
    
    % % Size
    sizes(nf, :) = [img.Height img.Width];
end

save('eye_images_sizes','sizes');

toc(t);
close(hw);