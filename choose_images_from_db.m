% script to help choosing images from helen
function chooseImagesFromDB

extension = '.jpg';

images_path = uigetdir();

% % Load images in test path
img_list=dir([images_path,'\*' extension]);
n_images = size(img_list,1);

deleted_images = cell(0);

% % Perform Fitting
for ii=1:n_images
   
    test_image = im2double(imread(fullfile(images_path,img_list(ii).name)));
    w = size(test_image, 2);
    
    max_size = 512;
    image = imresize(test_image, max_size/w);
    figure; imshow(image);
    movegui(gcf, 'west')
    
    % Construct a questdlg with thwo options
    choice = questdlg('Do you want to keep this image?', ...
        ['Choose images (' num2str(ii) '/' num2str(n_images) ')'], ...
        'Yes','No','Exit','Yes');
    
    
    % Handle response
    switch choice
        case 'Exit'
            close all
            return;
        case 'No'
            deleted_images{end+1} = img_list(ii).name;
            delete(fullfile(images_path,img_list(ii).name));
    end
    
    close all
end

save(fullfile(images_path, 'deleted_images'), 'deleted_images');