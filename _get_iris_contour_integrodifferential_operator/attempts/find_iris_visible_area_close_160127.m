function [visible_area, visible_area_pcnt, iris_visible_mask, iris_visible_out] = find_iris_visible_area(img, iris_mask, lm)
debug = false;
% save test

% load test

% Apply mask
iris = rgb2gray(img); % .* uint8(iris_mask);
% figure,
% subplot(4,1,1), imshow(iris)
% subplot(4,1,2), imshow(img(:,:,1))
% subplot(4,1,3), imshow(img(:,:,2))
% subplot(4,1,4), imshow(img(:,:,3))

% Crop iris and mask
[~, y1] = find(iris_mask, 1, 'first');
[~, y2] = find(iris_mask', 1, 'first');
[~, y3] = find(iris_mask, 1, 'last');
[~, y4] = find(iris_mask', 1, 'last');
% y1 = lm(1,1);
% y3 = lm(4,1);
iris_ini = imcrop(iris, [y1 y2 y3-y1 y4-y2]);
iris_mask = imcrop(iris_mask, [y1 y2 y3-y1 y4-y2]);
img_out = imcrop(img, [y1 y2 y3-y1 y4-y2]);

% Choose between components
% figure, for im=1:3, subplot(3,1,im), imshow(img_out(:,:,im)), end;
% figure;
% subplot(3,2,1), imshow(iris_mask.*edge(iris_ini, 'Canny')), title('Canny')
% subplot(3,2,2), imshow(iris_mask.*edge(iris_ini, 'log')), title('log')
% subplot(3,2,3), imshow(iris_mask.*edge(iris_ini, 'Prewitt')), title('Prewitt')
% subplot(3,2,4), imshow(iris_mask.*edge(iris_ini, 'Roberts')), title('Roberts')
% subplot(3,2,5), imshow(iris_mask.*edge(iris_ini, 'Sobel')), title('Sobel')
% subplot(3,2,6), imshow(iris_mask.*edge(iris_ini, 'zerocross')), title('zerocross')

% Remove specular reflections
% iris_wo_sr = remove_iris_specular_reflections(img_out, iris_mask);

iris_borders = iris_mask .* edge(iris_ini, 'Canny', [0.1 0.3], 2);

iris_closed = iris_borders; % not appropiate, joins blobs bwmorph(iris_borders, 'close');

iris_size = size(iris_closed);
props = regionprops(iris_closed, 'Perimeter', 'Centroid', 'BoundingBox', 'Eccentricity', 'PixelIdxList');

% Delete elements with eccentricity < 0.90
delete_by_eccentricity = [props(:).Eccentricity] < 0.9 & [props(:).Perimeter] < iris_size(2);
iris_wo_sr = deleteByIdx(iris_closed, props, delete_by_eccentricity);

% Delete elements closer than dmin pixels to the center of the image
dmin = 12.5;
iris_clean = deleteByPos(iris_wo_sr, props, dmin);

% Delete unnecessary elements (only closer edges to the pupil are useful)
iris_final_border = deleteUnnecessaryEdges(iris_clean);


iris_out = repmat(uint8(~iris_final_border),1,1,3) .* img_out;

figure,
subplot(3,2,1), imshow(iris_borders), title('iris borders')
subplot(3,2,2), imshow(iris_closed), title('iris closed')
subplot(3,2,3), imshow(iris_wo_sr), title('iris wo sr')
subplot(3,2,4), imshow(iris_clean), title('iris clean')
subplot(3,2,5), imshow(iris_final_border), title('iris final edge')
subplot(3,2,6), imshow(iris_out), title('iris out')


visible_area = iris_borders;
visible_area_pcnt = iris_wo_sr;
iris_visible_mask = iris_final_border;
iris_visible_out = iris_out;
return

figure;
subplot(2,2,1), imshow(iris_borders);
subplot(2,2,2), imshow(iris_borders_ok);
subplot(2,2,3), imshow(iris_visible_border);

% Remove specular reflections ( TO BE IMPROVED )
iris_f = imcomplement(imfill(imcomplement(iris_ini), 'holes'));
iris_s = imcomplement(iris_ini-iris_f).*uint8(iris_mask);
% iris_sbw = uint8(im2bw(iris_s, 0.9));
iris_rem_spec = close_holes(iris_s)-iris_s;
iris = iris_ini - 2*iris_rem_spec;

% Close holes and remanent
iris_ch = close_holes(iris) - iris;

% Apply iris mask
iris_ch_mask = iris_ch .* uint8(iris_mask);

% Keep largest shape
labels = bwlabel(iris_ch_mask);
areas = regionprops(labels, 'Area', 'Centroid');
[~, order] = sort([areas(:).Area], 'descend');
iris_largest = ismember(labels, order(1));

% Fill the holes
iris_fill = imfill(iris_largest, 'holes');

% Smooth shape
iris_visible_mask = imclose(iris_fill, strel('disk', 1));

% Apply iris visible mask to color image
iris_mask_3d = uint8(repmat(iris_visible_mask, 1, 1, 3));
iris_visible_out = img_out .* iris_mask_3d;

% Visible area
visible_area = sum(iris_visible_mask(:));
visible_area_pcnt = visible_area / sum(iris_mask(:));

if(debug)
    figure
    subplot(1,2,1), imshowpair(iris_ini, iris_visible_mask)
    subplot(1,2,2), imshow(iris_ini.*uint8(iris_visible_mask))
end


function img = deleteByIdx(img, s, idxs)
idxs = find(idxs);
n_idx = numel(idxs);
for n=1:n_idx
    img(s(idxs(n)).PixelIdxList)=0;
end


function [img, dmin] = deleteByPos(img, s, th)
[h, w] = size(img);
p_ref = [h/2 w/2];
n_rois = numel(s);
dmin = zeros(n_rois, 1);
for nr=1:n_rois
    [i, j] = ind2sub([h w], s(nr).PixelIdxList);
    points = [i j];
    d = sqrt((points(:,1)-p_ref(1)).^2 + (points(:,2)-p_ref(2)).^2);
    dmin(nr) = min(d);
    
    if(dmin(nr) < th) % A�ADIR PARA QUE COMRPUEBE QUE NO ES UN BORDE
        img(s(nr).PixelIdxList) = 0;
    end
    
    if(false)
        figure;imshow(img);
        hold on
        plot(p_ref(:,2), p_ref(:,1), 'y*');
        plot(points(:,2), points(:,1), 'r.');
    end
end

function imgo = deleteUnnecessaryEdges(img)
[h, w] = size(img);
middle = floor(h/2);
imgo = false(h, w);

[i, j] = find(img);
ir = i - middle;

[jo, ji] = sort(j);
io = ir(ji);

labels = unique(jo);
n_labels = numel(labels);

for nl=1:n_labels
    col = labels(nl);
    icol = io(jo == col);
    if(sum(diff(icol))+1 ~= numel(icol)) % if line is not continuos, keep only closest edges (up and down)
        up = min(icol(icol >= 0));
        down = max(icol(icol < 0));
        row_up = up + middle;
        ind_up = sub2ind([h, w], row_up, repmat(col, numel(row_up), 1));
        row_down = down + middle;
        ind_down = sub2ind([h, w], row_down, repmat(col, numel(row_down), 1));
        if(exist('up', 'var')), imgo(ind_up) = true; end
        if(exist('down', 'var')), imgo(ind_down) = true; end
    else
        row = icol+middle;
        ind = sub2ind([h, w], row, repmat(col, numel(row), 1));
        imgo(ind) = true;
    end
end