function [visible_area, visible_area_pcnt, iris_visible_mask, iris_visible_out] = find_iris_visible_area(img, iris_mask, lm)
debug = false;
% save test

% load test

% Apply mask
iris = rgb2gray(img); % .* uint8(iris_mask);
% figure,
% subplot(4,1,1), imshow(iris)
% subplot(4,1,2), imshow(img(:,:,1))
% subplot(4,1,3), imshow(img(:,:,2))
% subplot(4,1,4), imshow(img(:,:,3))

% Crop iris and mask
[~, y1] = find(iris_mask, 1, 'first');
[~, y2] = find(iris_mask', 1, 'first');
[~, y3] = find(iris_mask, 1, 'last');
[~, y4] = find(iris_mask', 1, 'last');
% y1 = lm(1,1);
% y3 = lm(4,1);
iris_ini = imcrop(iris, [y1 y2 y3-y1 y4-y2]);
iris_mask = imcrop(iris_mask, [y1 y2 y3-y1 y4-y2]);
img_out = imcrop(img, [y1 y2 y3-y1 y4-y2]);

[h, w] = size(iris_ini);

% Choose between components
% figure, for im=1:3, subplot(3,1,im), imshow(img_out(:,:,im)), end;
% figure;
% subplot(3,2,1), imshow(iris_mask.*edge(iris_ini, 'Canny')), title('Canny')
% subplot(3,2,2), imshow(iris_mask.*edge(iris_ini, 'log')), title('log')
% subplot(3,2,3), imshow(iris_mask.*edge(iris_ini, 'Prewitt')), title('Prewitt')
% subplot(3,2,4), imshow(iris_mask.*edge(iris_ini, 'Roberts')), title('Roberts')
% subplot(3,2,5), imshow(iris_mask.*edge(iris_ini, 'Sobel')), title('Sobel')
% subplot(3,2,6), imshow(iris_mask.*edge(iris_ini, 'zerocross')), title('zerocross')

% Remove specular reflections
% iris_wo_sr = remove_iris_specular_reflections(img_out, iris_mask);

iris_borders = logical(iris_mask .* edge(iris_ini, 'Canny', [0.1 0.3], 2));

iris_size = size(iris_borders);
props = regionprops(iris_borders, 'Perimeter', 'Centroid', 'BoundingBox', 'Eccentricity', 'PixelIdxList');

% Delete elements with eccentricity < 0.90 and perimeter < iris width 
% (to avoid deleting the iris contour, which could have eccentricity ~ 0)
delete_by_eccentricity = [props(:).Eccentricity] < 0.9 & [props(:).Perimeter] < iris_size(2);
iris_clean_sr = deleteByIdx(iris_borders, props, delete_by_eccentricity);

% Delete elements closer than dmin pixels to the center of the image
dmin = 12.5;
iris_clean_closer = deleteByPos(iris_clean_sr, props, dmin);

% Delete unnecessary elements (only closer edges to the pupil are useful)
iris_clean_duplicates = deleteUnnecessaryEdges(iris_clean_closer);

% Create a closed region based on edge detections
iris_close = imclose(iris_clean_duplicates, strel('disk', 10));
iris_final = imcomplement(imfill(iris_close, [1 1; h 1; 1 w; h w]));

iris_out = repmat(uint8(iris_final),1,1,3) .* img_out;

figure,
ax1 = subplot(3,3,1), imshow(iris_borders), title('iris borders');
ax2 = subplot(3,3,2), imshow(iris_clean_sr), title('iris clean sr');
ax3 = subplot(3,3,3), imshow(iris_clean_closer), title('iris clean closer');
ax4 = subplot(3,3,4), imshow(iris_clean_duplicates), title('iris clean duplicates');
ax5 = subplot(3,3,5), imshow(iris_close), title('iris close');
ax6 = subplot(3,3,6), imshow(iris_final), title('iris final');
ax7 = subplot(3,3,7), imshow(iris_out), title('iris out');
linkaxes([ax1 ax2 ax3 ax4 ax5 ax6 ax7]);

visible_area = iris_borders;
visible_area_pcnt = iris_clean_duplicates;
iris_visible_mask = iris_close;
iris_visible_out = iris_out;
return

figure;
subplot(2,2,1), imshow(iris_borders);
subplot(2,2,2), imshow(iris_borders_ok);
subplot(2,2,3), imshow(iris_visible_border);

% Remove specular reflections ( TO BE IMPROVED )
iris_f = imcomplement(imfill(imcomplement(iris_ini), 'holes'));
iris_s = imcomplement(iris_ini-iris_f).*uint8(iris_mask);
% iris_sbw = uint8(im2bw(iris_s, 0.9));
iris_rem_spec = close_holes(iris_s)-iris_s;
iris = iris_ini - 2*iris_rem_spec;

% Close holes and remanent
iris_ch = close_holes(iris) - iris;

% Apply iris mask
iris_ch_mask = iris_ch .* uint8(iris_mask);

% Keep largest shape
labels = bwlabel(iris_ch_mask);
areas = regionprops(labels, 'Area', 'Centroid');
[~, order] = sort([areas(:).Area], 'descend');
iris_largest = ismember(labels, order(1));

% Fill the holes
iris_fill = imfill(iris_largest, 'holes');

% Smooth shape
iris_visible_mask = imclose(iris_fill, strel('disk', 1));

% Apply iris visible mask to color image
iris_mask_3d = uint8(repmat(iris_visible_mask, 1, 1, 3));
iris_visible_out = img_out .* iris_mask_3d;

% Visible area
visible_area = sum(iris_visible_mask(:));
visible_area_pcnt = visible_area / sum(iris_mask(:));

if(debug)
    figure
    subplot(1,2,1), imshowpair(iris_ini, iris_visible_mask)
    subplot(1,2,2), imshow(iris_ini.*uint8(iris_visible_mask))
end


function img = deleteByIdx(img, s, idxs)
idxs = find(idxs);
n_idx = numel(idxs);
for n=1:n_idx
    img(s(idxs(n)).PixelIdxList)=0;
end


function [img, dmin] = deleteByPos(img, s, th_dmin)
[h, w] = size(img);
p_ref = [h/2 w/2];
n_rois = numel(s);
dmin = zeros(n_rois, 1);
for nr=1:n_rois
    [i, j] = ind2sub([h w], s(nr).PixelIdxList);
    points = [i j];
    d = sqrt((points(:,1)-p_ref(1)).^2 + (points(:,2)-p_ref(2)).^2);
    dmin(nr) = min(d);
    ratio = s(nr).BoundingBox(3)/s(nr).BoundingBox(4);
    
    % Delete all regions that are closer than th_dmin to the image's center
    % ONLY the ones which aspect ratio > 0.5 => close to squared bounding box
    % to avoid iris border deletion
    if(dmin(nr) < th_dmin && ratio > 0.5)
        img(s(nr).PixelIdxList) = 0;
    end
    
    if(false)
        figure;imshow(img);
        hold on
        plot(p_ref(:,2), p_ref(:,1), 'y*');
        plot(points(:,2), points(:,1), 'r.');
    end
end

function imgo = deleteUnnecessaryEdges(img)
[h, w] = size(img);
middle = floor(h/2);
imgo = false(h, w);

[i, j] = find(img);
ir = i - middle;

[jo, ji] = sort(j);
io = ir(ji);

cols = unique(jo);
n_cols = numel(cols);

for nl=1:n_cols
    col = cols(nl);
    icol = io(jo == col);
    % lower half of the image (from -1 to bottom)
    icoldown = icol(icol >= 0); % find borders in lower rows in column 'col'
    % if there ARE borders...
    if(~isempty(icoldown))
        % check if there is a continuos border going down
        % if there is not, keep only the closest point and all connected
        % vertically to it.
        icoldowndiff = diff(icoldown);
        if(sum(icoldowndiff)+1 ~= numel(icoldown))
            % if closest point has any vertically connected to it, select them
            % but only those connected to closest point, not those connected
            % between them but not to the closest point
            connected = logical([0; icoldowndiff == 1]);
            last_connected = find(connected == 0, 2, 'first');
            connected(last_connected(2)+1:end) = 0;
            down = [min(icoldown); icoldown(connected)];
            row_down = down + middle;
            ind_down = sub2ind([h, w], row_down, repmat(col, numel(row_down), 1));
            if(exist('down', 'var')), imgo(ind_down) = true; end
        else
            % else, if there is only one continuous line going down, keep all of it
            row_down = icoldown+middle;
            ind_down = sub2ind([h, w], row_down, repmat(col, numel(row_down), 1));
            imgo(ind_down) = true;
        end
    end
    
    % upper half of the image (from 0 to top)
    icolup = icol(icol < 0); % find borders in upper rows in column 'col'
    % if there ARE borders...
    if(~isempty(icolup))
        % check if there is a continuos border going up
        % if there is not, keep only the closest point and all connected
        % vertically to it.
        icolupdiff = diff(icolup);
        if(sum(icolupdiff)+1 ~= numel(icolup))
            % if closest point has any vertically connected to it, select them
            % but only those connected to closest point, not those connected
            % between them but not to the closest point
            connected = logical([icolupdiff == 1; 0]);
            last_connected = find(connected(1:end-1) == 0, 1, 'last');
            connected(1:last_connected) = 0;
            up = [icolup(connected); max(icolup)];
            row_up = up + middle;
            ind_up = sub2ind([h, w], row_up, repmat(col, numel(row_up), 1));
            if(exist('up', 'var')), imgo(ind_up) = true; end
        else
            % else, if there is only one continuous line going up, keep all of it
            row_up = icolup+middle;
            ind_up = sub2ind([h, w], row_up, repmat(col, numel(row_up), 1));
            imgo(ind_up) = true;
        end
    end
end