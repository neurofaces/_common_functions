function [visible_area, visible_area_pcnt, eyelid] = find_iris_visible_area(img, iris_mask, lm)
debug = true;

% Apply mask
iris = img; % .* uint8(iris_mask);

% Crop iris and mask
[~, y1] = find(iris_mask, 1, 'first');
[~, y2] = find(iris_mask', 1, 'first');
[~, y3] = find(iris_mask, 1, 'last');
[~, y4] = find(iris_mask', 1, 'last');
y1 = lm(1,1);
y3 = lm(4,1);
iris_ini = imcrop(iris, [y1 y2 y3-y1 y4-y2]);
iris_mask = imcrop(iris_mask, [y1 y2 y3-y1 y4-y2]);

% Remove specular reflections
iris = imcomplement(imfill(imcomplement(iris_ini), 'holes'));

% Threshold image to extract eyelid
eyelid = im2bw(iris, graythresh(iris));

% Visible area
visible_area = sum(iris_mask(:))-sum(eyelid(:));
visible_area_pcnt = visible_area / sum(iris_mask(:));

if(debug)
    figure
    subplot(1,2,1), imshowpair(iris_ini, eyelid)
    subplot(1,2,2), imshow(iris_ini.*uint8(imcomplement(eyelid)))
end

