function [visible_area, visible_area_pcnt, iris_visible_mask, iris_visible_out] = find_iris_visible_area_otsu(img, iris_mask, lm)
debug = false;


irisr = img(:,:,1);
if(debug), figure, subplot(3,3,1), imshow(irisr), title('original'), end;
[h, w] = size(irisr);

% Remove specular reflections
iris_wo_sr = imcomplement(close_holes(imcomplement(irisr)));
if(debug), subplot(3,3,2), imshow(iris_wo_sr); title('iris wo sr'); end;

% Calculate the residue (image with holes closed - original image)
iris_res = close_holes(iris_wo_sr) - iris_wo_sr;
if(debug), subplot(3,3,3), imshow(iris_res); title('iris res'); end;

% Apply median filter to the residue
iris_med = medfilt2(iris_res, [10 10]);
if(debug), subplot(3,3,4), imshow(iris_med); title('iris med'); end;

% Perform closing on median filtered iris
iris_close = imclose(iris_med, strel('disk', 10));
if(debug), subplot(3,3,5), imshow(iris_close); title('iris closed'); end;

% Binarize using otsu method to obtain the threshold
iris_otsu = im2bw(iris_close, graythresh(iris_close));
if(debug), subplot(3,3,6), imshow(iris_otsu); title('iris otsu'); end;

% Apply mask to binarized image to keep only the iris area
iris_visible_mask = iris_otsu & iris_mask;
if(debug), subplot(3,3,7), imshow(iris_visible_mask); title('iris visible mask'); end;

% Get the border of the detected area
iris_visible_border = iris_visible_mask - imerode(iris_visible_mask, strel('disk', 1));

% Create a 3D mask to apply to the RGB image
iris_mask_3d = uint8(repmat(iris_visible_mask, 1, 1, 3));

% Create a yellow 3D border to apply to the RGB image
border = uint8(iris_visible_border);
border(border==1)=255;
border(border==0)=1;
iris_border_3d = repmat(border, 1, 1, 2);
iris_border_3d(:,:,3) = ones(h, w);

% Apply border to the RGB original image
iris_out = img .* iris_border_3d;
if(debug), subplot(3,3,8), imshow(iris_out); end;

visible_area = iris_res;
visible_area_pcnt = iris_otsu;
% iris_visible_mask = iris_visible_mask;
iris_visible_out = iris_out;
return


% % Keep largest shape
% labels = bwlabel(iris_ch_mask);
% areas = regionprops(labels, 'Area', 'Centroid');
% [~, order] = sort([areas(:).Area], 'descend');
% iris_largest = ismember(labels, order(1));
% 
% % Fill the holes
% iris_fill = imfill(iris_largest, 'holes');
% 
% % Smooth shape
% iris_visible_mask = imclose(iris_fill, strel('disk', 1));
% 
% % Apply iris visible mask to color image
% iris_mask_3d = uint8(repmat(iris_visible_mask, 1, 1, 3));
% iris_visible_out = img_out .* iris_mask_3d;
% 
% % Visible area
% visible_area = sum(iris_visible_mask(:));
% visible_area_pcnt = visible_area / sum(iris_mask(:));
% 
% if(debug)
%     figure
%     subplot(1,2,1), imshowpair(iris_ini, iris_visible_mask)
%     subplot(1,2,2), imshow(iris_ini.*uint8(iris_visible_mask))
% end


