% function [visible_area, visible_area_pcnt, iris_visible_mask, iris_visible_out] = find_iris_visible_area(img, iris_mask, lm)
% debug = false;
% save test

load test

% Apply mask
iris = rgb2gray(img); % .* uint8(iris_mask);
figure, 
subplot(4,1,1), imshow(iris)
subplot(4,1,2), imshow(img(:,:,1))
subplot(4,1,3), imshow(img(:,:,2))
subplot(4,1,4), imshow(img(:,:,3))

% Crop iris and mask
[~, y1] = find(iris_mask, 1, 'first');
[~, y2] = find(iris_mask', 1, 'first');
[~, y3] = find(iris_mask, 1, 'last');
[~, y4] = find(iris_mask', 1, 'last');
y1 = lm(1,1);
y3 = lm(4,1);
iris_ini = imcrop(iris, [y1 y2 y3-y1 y4-y2]);
iris_mask = imcrop(iris_mask, [y1 y2 y3-y1 y4-y2]);
img_out = imcrop(img, [y1 y2 y3-y1 y4-y2]);

% Choose between components
% figure, for im=1:3, subplot(3,1,im), imshow(img_out(:,:,im)), end;

% Remove specular reflections ( TO BE IMPROVED )
iris_f = imcomplement(imfill(imcomplement(iris_ini), 'holes'));
iris_s = imcomplement(iris_ini-iris_f).*uint8(iris_mask);
% iris_sbw = uint8(im2bw(iris_s, 0.9));
iris_rem_spec = close_holes(iris_s)-iris_s;
iris = iris_ini - 2*iris_rem_spec;

% Close holes and remanent
iris_ch = close_holes(iris) - iris;

% Apply iris mask
iris_ch_mask = iris_ch .* uint8(iris_mask);

% Keep largest shape
labels = bwlabel(iris_ch_mask);
areas = regionprops(labels, 'Area', 'Centroid');
[~, order] = sort([areas(:).Area], 'descend');
iris_largest = ismember(labels, order(1));

% Fill the holes
iris_fill = imfill(iris_largest, 'holes');

% Smooth shape
iris_visible_mask = imclose(iris_fill, strel('disk', 1));

% Apply iris visible mask to color image
iris_mask_3d = uint8(repmat(iris_visible_mask, 1, 1, 3));
iris_visible_out = img_out .* iris_mask_3d;

% Visible area
visible_area = sum(iris_visible_mask(:));
visible_area_pcnt = visible_area / sum(iris_mask(:));

if(debug)
    figure
    subplot(1,2,1), imshowpair(iris_ini, iris_visible_mask)
    subplot(1,2,2), imshow(iris_ini.*uint8(iris_visible_mask))
end

