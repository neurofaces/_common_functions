% Function to detect the pupil boundary
% Search a certain subset of the image with a given radius range(rmin,rmax)
% around a 10*10 neighbourhood of the point x,y given as input
% INPUTS:
% I: input image
% C: centre coordinates
% rmin, rmax: minimum and maximum radius values
% n: number of sides of the polygon (for lineint)
% part: specifies whether it is searching for the iris or pupil
% sigma: standard deviation of the gaussian
% OUTPUT:
% cp: centre coordinates of pupil(x,y) followed by radius
% Author: Anirudh S.K.
% Department of Computer Science and Engineering
% Indian Institute of Techology, Madras
% Modified by:  F�lix F.H.
%               Polytechnical University of Valencia

function cp = search(I, C, rmin, rmax, sigma, n, option)
try
x = C(1);
y = C(2);
rows = size(I, 1);
cols = size(I, 2);
maxb = zeros(rows, cols);
maxrad = zeros(rows, cols);
for i=(x-5):(x+5)
    for j=(y-5):(y+5)
        [b, r, ~] = partiald(I, [i j], rmin, rmax, sigma, n, option);
        maxb(i, j) = b;
        maxrad(i, j) = r;
    end
end
[X, Y] = find(maxb == max(maxb(:)));
radius = maxrad(X, Y);
cp = [X Y radius];
catch e
    disp(e.getReport());
end