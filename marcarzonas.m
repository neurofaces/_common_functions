function immarc=marcarzonas(im,MS)

if(iscell(MS))
    n_markers = numel(MS);
    M = MS;
else
    n_markers = 1;
    M{1} = MS;
end

r = [255 0 0 255 255 0 255];
g = [0 255 0 128 255 255 0];
b = [0 0 255 0 0 255 255];

tmn=size(im);
immarc=zeros(tmn(1),tmn(2),3);
immarc=im;
immarc(:,:,2)=im;
immarc(:,:,3)=im;

for nm=1:n_markers
    [f,c]=find(M{nm}==1);
    [filas,colum]=size(im);
    coordR=(c-1)*filas+f;
    immarc(coordR)=r(nm);
    immarc(coordR+filas*colum)=g(nm);
    immarc(coordR+filas*colum*2)=b(nm);
end