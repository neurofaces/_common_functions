function d = dist_point_to_point(p1, p2)

d = sqrt(sum((p2-p1).^2));