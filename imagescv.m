function imagescv(mat, decimals)

if(nargin < 2), decimals = 2; end;

if(decimals > 0)
    rep = ['%0.' num2str(decimals) 'f'];
else
    rep = '%d';
end

nel = size(mat, 2);

% if mat is square
if(nel == size(mat,1))

    % mat = rand(5);           %# A 5-by-5 matrix of random values from 0 to 1
    imagesc(mat);            %# Create a colored plot of the matrix values
    colormap(flipud(gray));  %# Change the colormap to gray (so higher values are
                             %#   black and lower values are white)
    colorbar

    textStrings = num2str(mat(:), rep);  %# Create strings from the matrix values
    textStrings = strtrim(cellstr(textStrings));  %# Remove any space padding
    [x,y] = meshgrid(1:nel);   %# Create x and y coordinates for the strings
    hStrings = text(x(:),y(:),textStrings(:),...      %# Plot the strings
                    'HorizontalAlignment','center');
    midValue = mean(get(gca,'CLim'));  %# Get the middle value of the color range
    textColors = repmat(mat(:) > midValue,1,3);  %# Choose white or black for the
                                                 %#   text color of the strings so
                                                 %#   they can be easily seen over
                                                 %#   the background color
    set(hStrings,{'Color'},num2cell(textColors,2));  %# Change the text colors

    set(gca,'XTick',1:nel,...                         %# Change the axes tick marks
            'XTickLabel',1:nel,...  %#   and tick labels
            'YTick',1:nel,...
            'YTickLabel',1:nel,...
            'TickLength',[0 0]);
        
else
    
    imagesc(mat);            %# Create a colored plot of the matrix values
    colormap(flipud(gray));  %# Change the colormap to gray (so higher values are
                             %#   black and lower values are white)
    colorbar

    textStrings = num2str(mat(:),'%0.2f');  %# Create strings from the matrix values
    textStrings = strtrim(cellstr(textStrings));  %# Remove any space padding
    x = 1:nel; %# Create x and y coordinates for the strings
    y = ones(1, nel);
    hStrings = text(x(:),y(:),textStrings(:),...      %# Plot the strings
                    'HorizontalAlignment','center');
    midValue = mean(get(gca,'CLim'));  %# Get the middle value of the color range
    textColors = repmat(mat(:) > midValue,1,3);  %# Choose white or black for the
                                                 %#   text color of the strings so
                                                 %#   they can be easily seen over
                                                 %#   the background color
    set(hStrings,{'Color'},num2cell(textColors,2));  %# Change the text colors

    set(gca,'XTick',1:nel,...                         %# Change the axes tick marks
            'XTickLabel',1:nel,...  %#   and tick labels
            'YTick',1:nel,...
            'YTickLabel',1:nel,...
            'TickLength',[0 0]);
    set(gca, 'Position', [0.05 0.5 0.8 0.15]);
end