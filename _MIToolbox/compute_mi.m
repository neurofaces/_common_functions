function mimatrix = compute_mi(m)

nm = size(m, 2);
mimatrix = zeros(nm);

for r=1:nm
    for c=1:nm
        mimatrix(r, c) =  MutualInfo_mex(m(:,r)./max(abs(m(:,r))), m(:,c)./max(abs(m(:,c))));
    end
end