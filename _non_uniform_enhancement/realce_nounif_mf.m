function imout=realce_nounif_m(im, r, mask, L, verbose)

im = double(im);

imout = im;
mask = mask>0;

pixok = find(mask);
pixnook = find(1-mask);
imout(pixnook) = 0;

tmax = max(im(pixok));
tmin = min(im(pixok));

umax=255;
umin=0;

imop = imopen(im,strel('disk',5));

if(verbose), disp('mediablkf.m'); tic; end

[uf, maxim, minim] = mediablkf(imop,mask,L);

if(verbose), toc; end

md = maxim-minim;

indm=find(im <= uf & mask);
indM=find(im > uf & mask);

n_indm = numel(indm);
n_indM = numel(indM);

maxInd = max(n_indm, n_indM);

for i=1:maxInd
    if(i <= n_indm && md(indm(i))>=20 && maxim(indm(i))>=15)
        imout(indm(i))=round(umin+(0.5*(umax-umin)*(im(indm(i))-tmin).^r)/(uf(indm(i))-tmin).^r);
    end

    if(i <= n_indM && md(indM(i))>=20 && maxim(indM(i))>=15)
        imout(indM(i))=round(umax+ (-0.5*(umax-umin))*(im(indM(i))-tmax).^r/(uf(indM(i))-tmax).^r);
    end
end
imout=uint8(imout);