function imout=realce_nounif_m(im,r,mask,L)

im = double(im);

imout = im;
mask = mask>0;

pixok = find(mask);
pixnook = find(1-mask);
imout(pixnook) = 0;

tmax = max(im(pixok));
tmin = min(im(pixok));

umax=255;
umin=0;

imop = imopen(im,strel('disk',5));

tic
[uf, maxim, minim] = mediablk(imop,mask,L);
toc

md = maxim-minim;

indm=find(im <= uf & mask);
indM=find(im > uf & mask);

for i=1:length(indm)
    if(md(indm(i))<20 && maxim(indm(i))<15)
        continue
    else
        imout(indm(i))=round(umin+(0.5*(umax-umin)*(im(indm(i))-tmin).^r)/(uf(indm(i))-tmin).^r);
    end
end
for i=1:length(indM)
    if(md(indM(i))<20 && maxim(indM(i))<15)
        continue
    else
        imout(indM(i))=round(umax+ (-0.5*(umax-umin))*(im(indM(i))-tmax).^r/(uf(indM(i))-tmax).^r);
    end
end
imout=uint8(imout);