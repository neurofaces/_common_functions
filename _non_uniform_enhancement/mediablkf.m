function [imMed, imMax, imMin] = mediablk(imin, mask, a)

% imout = mediablk(imin, mask, a)
%
% Esta funci�n promedia los puntos indicados por mask utilizando una
% ventana cuadrada de tama�o (2*a+1)x(2*a+1)

[M,N] = size(imin);
imin = double(imin);
%mask=double(mask==1);
imin = imin.*(mask>0);

%a = 9;
%L = 2*a + 1;

% a�ado bordes
imin = [zeros(a,N+2*a);
    zeros(M,a), imin, zeros(M,a);
    zeros(a, N+2*a)];

mask = [zeros(a,N+2*a);
    zeros(M,a), mask, zeros(M,a);
    zeros(a, N+2*a)];

imMed = imin;
imMax = imin;
imMin = imin;

% busco posiciones donde tengo que filtrar (donde mask=1) y filtro
[i,j] = find(mask>0);

for k=1:length(i)
    m = mask(i(k)+(-a:a),j(k)+(-a:a));
    sum_ones_in_mask = sum(m(:)>0);
    if sum_ones_in_mask % si hay algun pixel de la m�scara a uno
        x = imin(i(k)+(-a:a),j(k)+(-a:a));
        imMed(i(k),j(k))= sum(x(:))/sum_ones_in_mask;
        imMax(i(k),j(k)) = max(x(:));
        imMin(i(k),j(k)) = min(x(:));
    end
end
    
% elimino bordes
imMed = (imMed(a+1:end-a, a+1:end-a));
imMax = (imMax(a+1:end-a, a+1:end-a));
imMin = (imMin(a+1:end-a, a+1:end-a));
