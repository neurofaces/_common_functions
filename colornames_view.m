function colornames_view(scm,srt)
% Create an interactive figure for viewing all colorschemes in "colornames.mat".
%
% (c) 2015 Stephen Cobeldick
%
% Syntax:
%  colornames_view
%  colornames_view(scheme)
%  colornames_view(scheme,sort)
%
% Create a figure displaying all of the colors from any colorscheme provided
% by the file "colornames.mat". The scheme and sort order can be selected
% by drop-down menu or by optional inputs. The colors can be sorted:
% - alphabetically by colorname.
% - by color-space parameters: Lab, XYZ, YUV, HSV or RGB.
%
% The figure is resizeable (the colors flow to fit the axes) with a vertical
% scrollbar provided to view all of the colors (if required). A rudimentary
% zoom ability has also been implemented. Requires the file "colornames.mat".
%
% See also COLORNAMES PLOT PATCH SURF RGBPLOT COLORMAP BREWERMAP CUBEHELIX LBMAP CPRINTF
%
% ### Input Arguments ###
%
% Inputs (all inputs are optional):
%  scheme = String, the name of a color scheme contained in "colornames.mat".
%  sort  = String, the colorspace sort parameters, in the required order,
%          eg: 'RGB', 'BRG', 'GRB', 'Lab', 'abL', etc, OR 'Alphabetical'.
%
% colornames_view(scheme,sort)

persistent fgh axh slh csh sch txh txs rgb
%
% ### Parameters ###
%
% Text lightness threshold:
thr = 0.54;
%
% Text margin, uicontrol and axes gap (pixels):
mrg = 5;
gap = 4;
sid = 20;
%
% Slider position:
yid = 0;
ymx = 0;
%
data = load('colornames.mat');
%
% List of colorschemes:
mcs = fieldnames(data);
if nargin<1
    scm = mcs{1};
else
    assert(ischar(scm)&&isrow(scm),'First input <scheme> must be a string.')
    idz = strcmpi(scm,mcs);
    assert(any(idz),'First input must be one of:%s\b',sprintf(' %s,',mcs{:}))
    scm = mcs{idz};
end
%
% List of color sorting options:
trs = {'Lab','XYZ','YUV','HSV','RGB'};
trs = cellfun(@(s)perms(s(end:-1:1)),trs,'UniformOutput',false);
trs = ['Alphabetical';cellstr(vertcat(trs{:}))];
if nargin<2
    srt = trs{1};
else
    assert(ischar(srt)&&isrow(srt),'Second input <sort> must be a string.')
    idz = strcmpi(srt,trs);
    assert(any(idz),'Second input must be one of:%s\b',sprintf(' %s,',trs{:}))
    srt = trs{idz};
end
%
% Color sorting index:
idx = 1:numel(data.(scm).names);
%
% ### Create a New Figure ###
%
if isempty(fgh)||~ishghandle(fgh)
    % Figure with zoom and pan functions:
    fgh = figure('HandleVisibility','callback', 'IntegerHandle','off',...
        'NumberTitle','off', 'Name','ColorNames Viewer', 'Color','white',...
        'Units','pixels', 'ResizeFcn',@cnvResize);
    %
    set(zoom(fgh), 'ActionPostCallback',@cnvZoomClBk);
    set(pan(fgh),  'ActionPostCallback',@cnvPanClBk);
    % Axes and scrolling slider:
    slh = uicontrol('Parent',fgh, 'Style','slider', 'Visible','off',...
        'Enable','off', 'Value',1, 'Min',0, 'Max',1,...
        'FontUnits','pixels', 'Units','pixels', 'Callback',@cnvSldClBk);
    axh = axes('Parent',fgh, 'Visible','off', 'Units','pixels',...
        'YDir','reverse', 'XTick',[], 'YTick',[], 'XLim',[0,1], 'YLim',[0,1]);
    % Color scheme and color sorting method drop-down menus:
    csh = uicontrol('Parent',fgh, 'Style','popupmenu', 'String',mcs,...
        'ToolTip','Color Scheme', 'Units','pixels', 'Callback',@cnvScmClBk);
    sch = uicontrol('Parent',fgh, 'Style','popupmenu', 'String',trs,...
        'ToolTip','Sort Colors', 'Units','pixels',  'Callback',@cnvSrtClBk);
end
set(csh,'Value',find(strcmpi(scm,mcs)));
set(sch,'Value',find(strcmpi(srt,trs)));
%
% ### Callback Functions ###
%
function cnvScmClBk(h,~) % Drop-Down Menu CallBack
    % Select a new colorscheme.
    scm = mcs{get(h,'Value')};
    set(slh, 'Value',1)
    cnvTxtDraw
    cnvSortBy
    cnvResize
end
function cnvSrtClBk(h,~) % Drop-Down Menu CallBack
    % Select the color sorting method.
    srt = trs{get(h,'Value')};
    cnvSortBy
    cnvResize
end
function cnvSldClBk(h,~) % Slider CallBack
    % Scroll the axes by changing the axes limits.
    tmp = diff(get(axh,'Ylim'));
    set(axh, 'Ylim',[0,tmp] + (ymx-tmp)*(1-get(h,'Value')));
end
function cnvZoomClBk(~,~) % Zoom CallBack
    % Change the font and margin sizes.
    tmp = diff(get(axh,'Ylim'));
    set(txh, 'FontSize',txs/tmp);
    set(txh, 'Margin',mrg/tmp);
end
function cnvPanClBk(~,~) % Pan CallBack
    % Move the scroll bar to match panning of the axes.
    tmp = get(axh,'Ylim');
    set(slh, 'Value',max(0,min(1,1-tmp(1)/(ymx-diff(tmp)))))
end
%
% ### Sort Colors ###
%
function cnvSortBy
    % Define the index for sorting the displayed colors.
    [tmp,ids] = sort(srt);
    switch tmp
        case sort('Alphabetical')
            idx = 1:numel(txh);
        case 'BGR' % RGB -> [3,2,1]
            [~,idx] = sortrows(rgb,ids([3,2,1]));
        case 'UVY' % YUV -> [2,3,1]
            mat = [0.299,0.587,0.114;-0.14713,-0.28886,0.436;0.615,-0.51499,-1.0001];
            mat = cnvInvGamma(rgb)*mat.';
            [~,idx] = sortrows(mat,ids([2,3,1]));
        case {'XYZ','Lab'} % -> [1,2,3]
            mat = [0.41245,0.35758,0.18049;0.21259,0.71516,0.07217;0.019297,0.11918,0.9505];
            mat = bsxfun(@rdivide,cnvInvGamma(rgb)*mat.',[0.950456,1,1.088754]);
            if strcmp(tmp,'Lab')
                K = mat>(6/29)^3;
                F = K.*mat.^(1/3) + ~K.*(mat*(29/6)^2/3+4/29);
                mat(:,2:3) = bsxfun(@times,[500,200],F(:,1:2)-F(:,2:3));
                mat(:,1) = 116*F(:,2) - 16;
            end
            [~,idx] = sortrows(mat,ids);
        case 'HSV' % -> [1,2,3]
            mat = cnvInvGamma(rgb);
            [V,X] = max(mat,[],2);
            S = V - min(mat,[],2);
            N = numel(S);
            L = N*mod(X+0,3) + (1:N).';
            R = N*mod(X+1,3) + (1:N).';
            H = mod(2*(X-1)+(mat(L)-mat(R))./S,6);
            S = S./V;
            S(V==0) = 0;
            H(S==0) = 0;
            [~,idx] = sortrows([H,S,V],ids);
        otherwise
            error('This sort is not supported. Please check the help.')
    end
end
%
% ### Re/Draw Text Strings ###
%
txf = @(s,b,c)text('Parent',axh, 'String',s, 'BackgroundColor',b, 'Color',c,...
    'Margin',mrg, 'Units','data', 'Interpreter','none', 'Clipping','on',...
    'VerticalAlignment','bottom', 'HorizontalAlignment','right');
function cnvTxtDraw
    % Delete any existing colors:
    delete(txh(ishghandle(txh)))
    % Calculate the text color:
    rgb = double(data.(scm).rgb) / data.(scm).scale;
    baw = thr>rgb*[0.298936;0.587043;0.114021];
    % Draw new colors in the axes:
    txh = cellfun(txf,data.(scm).names,num2cell(rgb,2),num2cell(baw(:,[1,1,1]),2));
    txs = get(txh(1),'FontSize');
end
%
% ### Color Position Calculation ###
%
function [txx,txy] = cnvJustify(pos,txe)
    % Calculate color lengths from text and margins:
    txl = 2*mrg+txe(idx,3);
    txc = cumsum(txl);
    txv = 1:numel(txc);
    % Preallocate position array:
    txm = mean(txl);
    out = zeros(ceil(1.1*[txc(end)/pos(3),pos(3)/txm]));
    % Loop over the color lengths:
    vec = txv(txc<=pos(3));
    tmp = 0;
    k = 0;
    while ~isempty(vec)
        k = k+1;
        out(k,1:numel(vec)) = txc(vec)-tmp;
        % Split into lines to fit axes width:
        tmp = txc(vec(end));
        vec = txv(tmp<txc&txc-tmp<=pos(3));
    end
    % Calculate X and Y positions for each color:
    [~,txy,txx] = find(out.');
    yid = txy(end);
    txy = txy*(2*mrg+max(txe(idx,4)));
    ymx = txy(end)/pos(4);
end
%
% ### Resize the Axes and UIControls, Move the Colors ###
%
function cnvResize(~,~)
    zoom(fgh,'out')
    set(slh, 'Value',1)
    set(axh, 'Ylim',[0,1])
    set(txh, 'Units','pixels', 'FontSize',txs, 'Margin',mrg)
    txe = cell2mat(get(txh,'Extent'));
    top = get(slh,'FontSize')*2;
    fgp = get(fgh,'Position'); % [left bottom width height]
    hgt = round(fgp(4)-3*gap-top);
    % Full-width axes, no scrollbar:
    pos = [gap,gap,fgp(3)-2*gap,hgt];
    [txx,txy] = cnvJustify(pos,txe);
    if txy(end)>hgt
        % Narrow axes, with scollbar:
        wid = fgp(3)-3*gap-sid;
        pos = [gap,gap,wid,hgt];
        [txx,txy] = cnvJustify(pos,txe);
        % Resize the scrollbar, adjust scroll steps:
        set(slh, 'SliderStep',max(0,min(1,[0.25,1]/(yid*(ymx-1)/ymx))),...
                 'Position',[2*gap+wid,gap,sid,hgt],...
                 'Visible','on', 'Enable','on', 'Value',1)
    else
        set(slh, 'Visible','off', 'Enable','off', 'Value',1)
    end
    % Resize the axes and drop-down menus:
    set(axh, 'Position',pos)
    uiw = (fgp(3)-gap)/2-gap;
    set(csh, 'Position',[1*gap+0,  fgp(4)-top-gap,uiw,top])
    set(sch, 'Position',[2*gap+uiw,fgp(4)-top-gap,uiw,top])
    % Move text strings to the correct positions:
    arrayfun(@(h,x,y)set(h,'Position',[x,y]),txh(idx),txx-mrg,pos(4)-txy+mrg);
    set(txh, 'Units','data')
end
%
% ### Initialize ###
%
cnvTxtDraw
cnvSortBy
cnvResize
%
end
%----------------------------------------------------------------------END:colornames_view
function rgb = cnvInvGamma(rgb)
idx = rgb <= 0.040448;
rgb(idx) = rgb(idx)/12.92;
rgb(~idx) = real(((rgb(~idx) + 0.055)/1.055).^2.4);
end
%----------------------------------------------------------------------END:cnvInvGamma