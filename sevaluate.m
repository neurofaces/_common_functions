function [Jaccard, Dice, rfp, rfn, err] = sevaluate(gt, msk)
% gets label matrix for one tissue in segmented and ground truth
% and returns the similarity indices
% gt is a tissue in gold truth
% msk is the same tissue in segmented image
% rfp false pasitive ratio
% rfn false negative ratio


try
    gt = logical(gt(:));
    msk = logical(msk(:));
    common = sum(gt & msk);
    union = sum(gt | msk);
    difference = xor(msk, gt);
    cm = sum(gt); % the number of voxels in m
    co = sum(msk); % the number of voxels in o
    Jaccard = common/union;
    Dice = (2*common)/(cm+co);
    rfp = (co-common)/cm;
    rfn = (cm-common)/cm;
    err = nnz(difference)/numel(msk);
catch e
    disp(e.getReport());
end