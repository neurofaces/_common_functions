function [JC,DC,Pr,Rc,E] = segeval(SM,GT)
% Script for evaluation of segmentation results
% INPUTS
% SM - Segmentation Mask matrix to evaluate
% GT - Ground Truth matrix

% OUTPUTS
% JC - Jaccard Index
% DC - Dice Index
% Pr - Precision (Positive Predictive Value)
% Rc - Recall (Sensitivity)
% E - Error (NICE-I contest)

% Change to vectorial format
SM = SM(:)>0;
GT = GT(:)>0;

% Jaccard and Dice
inter = sum(GT & SM); 
union = sum(GT | SM);
JC = inter/union;
DC = (2*inter)/(sum(GT)+sum(SM));

% Sensitivity and Specificity
CP = classperf(GT, SM,'Positive', 1, 'Negative', 0);
Pr = CP.PositivePredictiveValue;
Rc = CP.Sensitivity;

% Error NICE-I
difference = xor(SM, GT);
E = nnz(difference)/numel(SM);


