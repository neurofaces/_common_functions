function resized_img = resize_image_canvas(img, new_size, resize_from)
% Creates a new image of "new_size" size completing missing pixels by repeating the first/last row/column
% enlarge specifies the directions of the enlargement. possibilities:
% 	- center 
% 	- top
% 	- bottom
% 	- left
% 	- right
% 	- t-right (top right)
% 	- b-right (bottom right)
%	- t-left
% 	- b-left
% It works both in forward and backward direction.

H = new_size(1);
W = new_size(2);

% Resize
[h, w, D] = size(img);
dh = h-H;
dw = w-W;

% Correct height
if(dh > 0) % crop
    switch(resize_from)
		case {'center', 'left', 'right'}
			top = floor(dh/2);
            bottom = dh - top;
            new_img = img(1+top:end-bottom,:,:);
		case {'t-left', 'top', 't-right'}
            new_img = img(1:H,:,:);
		case {'b-left', 'bottom', 'b-right'}
			new_img = img(end-H+1:end,:,:);
    end
elseif(dh < 0) % enlarge
	% choose directions to enlarge
	switch(resize_from)
		case {'center', 'left', 'right'}
			top = floor(abs(dh)/2);
			bottom = abs(dh) - top;
			new_img = uint8(zeros(H,w,D));
			new_img(top+1:end-bottom,:,:) = img;
			new_img(1:top,:,:) = imgaussfilt(repmat(img(1,:,:), top, 1), 10);
			new_img(end-bottom+1:end,:,:) = imgaussfilt(repmat(img(end,:,:), bottom, 1), 10);
		case {'t-left', 'top', 't-right'}
			bottom = abs(dh);
			new_img = uint8(zeros(H,w,D));
			new_img(1:h,:,:) = img;
			new_img(h+1:end,:,:) = imgaussfilt(repmat(img(end,:,:), bottom, 1), 10);
		case {'b-left', 'bottom', 'b-right'}
			top = abs(dh);
			new_img = uint8(zeros(H,w,D));
			new_img(end-h+1:end,:,:) = img;
			new_img(1:top,:,:) = imgaussfilt(repmat(img(1,:,:), top, 1), 10);
	end
else
	new_img = img;
end
% Correct width
if(dw > 0) % crop
    switch(resize_from)
		case {'center', 'top', 'bottom'}
			left = floor(dw/2);
            right = dw - left;
            resized_img = new_img(:,1+left:end-right,:);
		case {'t-left', 'left', 'b-left'}
			resized_img = new_img(:,1:W,:);
		case {'t-right', 'right', 'b-right'}
			resized_img = new_img(:,end-W+1:end,:);
    end
elseif(dw < 0) % enlarge
	% choose directions to enlarge
	switch(resize_from)
		case {'center', 'top', 'bottom'}
			left = floor(abs(dw)/2);
			right = abs(dw) - left;
			resized_img = uint8(zeros(H,W,D));
			resized_img(:,left+1:end-right,:) = new_img;
			resized_img(:,1:left,:) = imgaussfilt(repmat(new_img(:,1,:), 1, left), 10);
			resized_img(:,end-right+1:end,:) = imgaussfilt(repmat(new_img(:,end,:), 1, right), 10);
		case {'t-left', 'left', 'b-left'}
			right = abs(dw);
			resized_img = uint8(zeros(H,W,D));
			resized_img(:,1:w,:) = new_img;
			resized_img(:,w+1:end,:) = imgaussfilt(repmat(new_img(:,end,:), 1, right), 10);
		case {'t-right', 'right', 'b-right'}
			left = abs(dw);
			resized_img = uint8(zeros(H,W,D));
			resized_img(:,end-w+1:end,:) = new_img;
			resized_img(:,1:left,:) = imgaussfilt(repmat(new_img(:,1,:), 1, left), 10);
	end
else
	resized_img = new_img;
end