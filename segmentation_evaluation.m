function seg_eval = segmentation_evaluation(folder_masks, folder_gts, filename_ext)

% some parameters
% folder_masks = 'D:\DOCTORADO\01_DATA\_NoGIT_common_dbs\cfd\iris_segmentation_results\mine_optimized\mask_30dx30dy\';
% folder_gts = 'D:\DOCTORADO\01_DATA\_NoGIT_common_dbs\cfd\iris_segmentation_results\mine_optimized\mask_30dx30dy\gt\';
% extension = '.bmp';

% list all 'extension' files in 'folder_imgs'
img_filenames = dir([folder_masks '*' extension]);
n_img_filenames = numel(img_filenames);

% % Variable to store metrics (Jaccard,Dice,rfp,rfn,err)
seg_eval = zeros(n_img_filenames, 5);

h = waitbar(0, 'Evaluating segmentation... 0%');
t=tic;
% start the loop for every image in the list
for nf=1:n_img_filenames
    
    waitbar(nf/n_img_filenames, h, sprintf('Evaluating segmentation... %.2f%%', nf/n_img_filenames*100));
    % disp(['Evaluating segmentation ' num2str(nf) '/' num2str(n_img_filenames)]);
    
    % % Image filename
    image_name = strrep(img_filenames(nf).name, extension, '');
    
    % % Load image
    msk = imread(fullfile(folder_masks, [image_name extension]));
    gt = imread(fullfile(folder_gts, [image_name extension]));
        
    % % Evaluate
    [Jaccard,Dice,rfp,rfn, err] = sevaluate(gt, msk);
    seg_eval(nf, 1) = Jaccard;
    seg_eval(nf, 2) = Dice;
    seg_eval(nf, 3) = rfp;
    seg_eval(nf, 4) = rfn;
    seg_eval(nf, 5) = err;
end
disp('Execution ended.');
toc(t)

save(fullfile(folder_masks, 'seg_eval.mat'), 'seg_eval');
close(h);