clear; close all;
addpath(genpath('.'));

% % Load Models
fitting_model='models/Chehra_f1.0.mat';
load(fitting_model);    

% % Test Path
image_path='test_images/landmarks-para-valery-chehra-detector/';
img_list=dir([image_path,'*.jpg']);

% % Perform Fitting
for i=1:size(img_list,1)

    % % Load Image
    test_image=im2double(imread([image_path,img_list(i).name]));
    imshow(test_image);hold on;
    
    disp(['Detecting Face in ' ,img_list(i).name]);
    faceDetector = vision.CascadeObjectDetector();
    bbox = step(faceDetector, test_image);
    test_image = insertShape(test_image,'rectangle',bbox);
    imshow(test_image)
    test_init_shape = InitShape(bbox,refShape, test_image);
    test_init_shape = reshape(test_init_shape,49,2);
    plot(test_init_shape(:,1),test_init_shape(:,2),'ro');
    
    if size(test_image,3) == 3
        test_input_image = im2double(rgb2gray(test_image));
    else
        test_input_image = im2double((test_image));
    end
    
    disp(['Fitting ' img_list(i).name]);    
    % % Maximum Number of Iterations 
    % % 3 < MaxIter < 7
    MaxIter=6;
    test_points = Fitting(test_input_image,test_init_shape,RegMat,MaxIter);
    
%     landmarks = zeros(size(test_image,1), size(test_image,2));
%     rows = round(test_points(:,2));
%     cols = round(test_points(:,1));
%     idx = rows + (cols-1)*size(test_image,1);
%     landmarks(idx) = 1;
%     imwrite(landmarks, strcat(image_path, strrep(img_list(i).name, '.jpg', ''), '_landmarks.jpg'));
    
    
    plot(test_points(:,1),test_points(:,2),'g*','MarkerSize',6);hold off;
    legend('Initialization','Final Fitting');
    set(gcf,'units','normalized','outerposition',[0 0 1 1]);
    pause;
    close all;
    
end
