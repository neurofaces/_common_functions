function outliers = detect_outliers(samples)

std_th = 3.5;
epsilon = 1e-6;

n_var = size(samples, 2);

for nv = 1 : n_var
    s = samples(:, nv);
    mu = mean(s);
    sigma = std(s);
    
    % if values are not standardized
    if(abs(mu) > epsilon || abs(sigma-1) > epsilon)
        outliers = bsxfun(@lt, mu + std_th*sigma, s) | bsxfun(@gt, mu - std_th*sigma, s);
    else
        % if they are standardized (z-score)
        outliers = abs(s) >= std_th;
    end
    
end